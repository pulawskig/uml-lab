#include <cstdlib>
#include <iostream>
#include <string>
#include <memory>

#include "Circle.h"
#include "Rectangle.h"
#include "CShape.h"

using namespace std;

void rysuj(const Shape* shape, const Point& first, const Point& second, double resX, double resY) {
	for (double j = first.getY(); j <= second.getY(); j += resY) {
		for (double i = first.getX(); i <= second.getX(); i += resX) {
			if (shape->isIn(Point(i, j))) {
				cout << "X";
			} else {
				cout << " ";
			}
		}
		cout << endl;
	}
}

int main(int argc, char** argv) {
	if (argc != 7) {
		cout << "Zle argumenty programu!" << endl;
		system("pause");
		return -1;
	}

	Point first(stod(string(argv[1])), stod(string(argv[2])));
	Point second(stod(string(argv[3])), stod(string(argv[4])));
	double resX = stod(string(argv[5]));
	double resY = stod(string(argv[6]));

	Rectangle r1(Point(-2, -2), 6, 4);
	Rectangle r2(Point(-2, -1), 2, 2);
	CShape cs1(Point(0, 0), make_shared<Rectangle>(r1), make_shared<Rectangle>(r2), DIFFERENCE);

	Circle c1(Point(-2, -2), 1);
	CShape cs2(Point(0, 0), make_shared<CShape>(cs1), make_shared<Circle>(c1), DIFFERENCE);

	Rectangle r3(Point(-2, -3.5), 8, 0.5);
	CShape cs3(Point(3, 0), make_shared<CShape>(cs2), make_shared<Rectangle>(r3), UNION);

	Circle c2(Point(10, -10), 3);
	CShape cs4(Point(0, 0), make_shared<CShape>(cs3), make_shared<Circle>(c2), UNION);

	rysuj(&cs4, first, second, resX, resY);

	system("pause");
	return 0;
}