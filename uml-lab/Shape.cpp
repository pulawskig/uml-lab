#include "Shape.h"
#include "Point.h"

Shape::Shape(const Point& _center) : center(_center) {

}

Shape::~Shape() {

}

void Shape::setCenter(Point p) {
	center = p;
}

Point Shape::getCenter() const {
	return center;
}
