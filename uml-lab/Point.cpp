#include <cmath>

#include "Point.h"

Point::Point(double _x, double _y) : x(_x), y(_y) {

}

Point::~Point() {

}

void Point::setX(double _x) {
	x = _x;
}

void Point::setY(double _y) {
	y = _y;
}

double Point::getX() const {
	return x;
}

double Point::getY() const {
	return y;
}

double Point::distanceTo(const Point& another) const {
	return std::sqrt(std::pow(another.getX() - x, 2) + std::pow(another.getY() - y, 2));
}

Point Point::operator+(const Point& another) const {
	return Point(x + another.getX(), y + another.getY());
}

Point Point::operator-(const Point& another) const {
	return Point(x - another.getX(), y - another.getY());
}