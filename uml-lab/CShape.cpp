#include "CShape.h"
#include "Point.h"

CShape::CShape(const Point& _center, const std::shared_ptr<Shape>& _first, const std::shared_ptr<Shape>& _second, const OperationType& _type) : Shape(_center), first(_first), second(_second), type(_type) {

}

CShape::~CShape() {

}

bool CShape::isIn(const Point& p) const {
	if(first == nullptr || second == nullptr)
		return false;

	Point p2 = p - center;

	bool a = first->isIn(p2);
	bool b = second->isIn(p2);

	switch (type) {
	case UNION:
		return a || b;
	case DIFFERENCE:
		return a && !b;
	case INTERSECTION:
		return a && b;
	case EXCLUSIVE_DIFFERENCE:
		return (a || b) && !(a && b);
	default:
		return false;
	}
}