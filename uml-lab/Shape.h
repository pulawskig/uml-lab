#pragma once
 
#include "Point.h"

class Shape {
protected:
	Point center;

public:
	Shape(const Point&);
	virtual ~Shape();

	virtual bool isIn(const Point&) const = 0;
	Point getCenter() const;
	void setCenter(Point);
};