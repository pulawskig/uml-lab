#include "Rectangle.h"
#include "Point.h"

Rectangle::Rectangle(const Point& _center, double _width, double _height) : Shape(_center), width(_width), height(_height) {

}

Rectangle::~Rectangle() {

}

bool Rectangle::isIn(const Point& p) const {
	double left = center.getX() - width / 2;
	double right = center.getX() + width / 2;
	double top = center.getY() - height / 2;
	double bottom = center.getY() + height / 2;

	return p.getX() >= left && p.getX() <= right && p.getY() >= top && p.getY() <= bottom;
}