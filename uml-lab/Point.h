#pragma once

class Point {
private:
	double x;
	double y;

public:
	Point(double, double);
	virtual ~Point();
	void setX(double);
	void setY(double);
	double getX() const;
	double getY() const;
	double distanceTo(const Point&) const;
	Point operator-(const Point&) const;
	Point operator+(const Point&) const;
};