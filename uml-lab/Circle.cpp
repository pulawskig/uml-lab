#include "Circle.h"
#include "Point.h"

Circle::Circle(const Point& _center, double _radius) : Shape(_center), radius(_radius) {

}

Circle::~Circle() {

}

bool Circle::isIn(const Point& p) const {
	return p.distanceTo(center) <= radius;
}

