#pragma once

#include "Shape.h"

class Rectangle : public Shape {
private:
	double width;
	double height;

public:
	Rectangle(const Point& = Point(0.0, 0.0), double = 1.0, double = 1.0);
	virtual ~Rectangle();
	bool isIn(const Point&) const override;
};
