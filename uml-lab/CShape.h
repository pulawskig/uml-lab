#pragma once

#include <memory>
#include "Shape.h"
#include "OperationType.h"

class CShape : public Shape {
private:
	std::shared_ptr<Shape> first;
	std::shared_ptr<Shape> second;
	OperationType type;

public:
	CShape(const Point&, const std::shared_ptr<Shape>&, const std::shared_ptr<Shape>&, const OperationType&);
	virtual ~CShape();
	bool isIn(const Point& p) const override;
};