#pragma once

#include "Shape.h"

class Circle : public Shape {
private:
	double radius;

public:
	Circle(const Point& = Point(0.0, 0.0), double = 1.0);
	virtual ~Circle();
	bool isIn(const Point&) const override;
};